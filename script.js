var DaysCheckboxEventHandler = {

  MAX_DAYS: 3,

  displayMessage: function (checkedBoxes) {
    var length = checkedBoxes.length;
    var message = 'Only ' + this.MAX_DAYS + ' days can be selected. You have already selected';
    var i;
    for (i = 0; i < length - 1; i++) {
      message += ' ' + checkedBoxes[i].parentElement.textContent + ',';
    }
    message = message.replace(/\,$/, '');
    message += ' and ' + checkedBoxes[length - 1].parentElement.textContent;
    alert(message);
  },

  clearOtherCheckboxes: function (checkedBoxes, that) {
    var numberOfCheckedBoxes = checkedBoxes.length;
    while (numberOfCheckedBoxes--) {
      if (checkedBoxes[numberOfCheckedBoxes] !== that) {
        checkedBoxes[numberOfCheckedBoxes].checked = false;
      }
    }
  },

  checkboxEventListener: function () {
    if (this.checked) {
      var checkedBoxes = document.querySelectorAll('input[type="checkbox"]:checked');
      if (this.id !== 'none') {
        document.getElementById('none').checked = false;
        var numberOfCheckedBoxes = checkedBoxes.length;
        if (numberOfCheckedBoxes > DaysCheckboxEventHandler.MAX_DAYS) {
          this.checked = false;
          checkedBoxes = document.querySelectorAll('input[type="checkbox"]:checked');
          DaysCheckboxEventHandler.displayMessage(checkedBoxes);
        }
      } else {
        DaysCheckboxEventHandler.clearOtherCheckboxes(checkedBoxes, this);
      }
    }
  },

  eventHandlerInstance: function () {
    var elements = Array.prototype.slice.call(document.querySelectorAll('input[type="checkbox"]'));
    var i = elements.length;
    while (i--) {
      elements[i].addEventListener('change', DaysCheckboxEventHandler.checkboxEventListener);
    }
  }

};

var daysCheckboxEventHandler = DaysCheckboxEventHandler.eventHandlerInstance;
this.addEventListener('load', daysCheckboxEventHandler);
